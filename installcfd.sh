#!/bin/bash
#check if root
if (( EUID != 0 )); then
    echo "Run as root please." 1>&2
    exit 100
fi

# install cloudflared
curl -L https://bin.equinox.io/c/VdrWdbjqyF/cloudflared-stable-linux-amd64.deb --output /tmp/cloudflared.deb
apt-get install -y /tmp/cloudflared.deb

# install the cloudflared and updater services
echo "
[Unit]
Description=DNS over HTTPS (DoH) proxy client
Wants=network-online.target nss-lookup.target
Before=nss-lookup.target
[Service]
AmbientCapabilities=CAP_NET_BIND_SERVICE
CapabilityBoundingSet=CAP_NET_BIND_SERVICE
DynamicUser=yes
ExecStart=/usr/local/bin/cloudflared proxy-dns
[Install]
WantedBy=multi-user.target
EOF
sudo systemctl enable --now cloudflared-proxy-dns
" > /etc/systemd/system/cloudflared-proxy-dns.service
echo "
[Unit]
Description=cloudflared-proxy-dns-update timer
[Timer]
OnCalendar=daily
[Install]
WantedBy=timers.target
" > /etc/systemd/system/cloudflared-proxy-dns-update.timer
echo "
[Unit]
Description=cloudflared-proxy-dns-update
After=network.target
[Service]
ExecStart=/bin/bash -c '/usr/local/bin/cloudflared update; code=$?; if [ $code -eq 64 ]; then systemctl restart cloudflared; exit 0; fi; exit $code'
" > /etc/systemd/system/cloudflared-proxy-dns-update.service

# disable systemd-resolved from startup
systemctl disable systemd-resolved
# stop systemd-resolved service
systemctl stop systemd-resolved
# enable cloudflared on system start
systemctl enable cloudflared-proxy-dns.service
# start cloudflared
systemctl start cloudflared-proxy-dns.service
#enable and start cloudflared updater
systemctl enable cloudflared-proxy-dns-update.timer
systemctl start cloudflared-proxy-dns-update.timer
sudo systemctl daemon-reload

rm /tmp/cloudflared.deb

echo "nameserver 127.0.0.1" > /etc/resolv.conf
